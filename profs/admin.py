from django.contrib import admin
from .models import Professional

class ProfessionalAdmin(admin.ModelAdmin):
    list_display = ('brand_name', 'expire_date', 'telephone')

admin.site.register(Professional, ProfessionalAdmin)