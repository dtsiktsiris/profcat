# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-12 15:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profs', '0008_professional_update_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professional',
            name='update_date',
            field=models.DateTimeField(verbose_name='Ενημέρωση νέων'),
        ),
    ]
