from django.conf.urls import url

from . import views

app_name = 'profs'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^check_login/', views.check_login, name='check_login'),
    url(r'^logout_view/', views.logout_view, name='logout_view'),
    url(r'^(?P<url_name_para>[a-zA-Z]+)/edit_news/$', views.edit_news, name='edit_news'),
    url(r'^(?P<url_name_para>[a-zA-Z]+)/$', views.show, name='show'),
]