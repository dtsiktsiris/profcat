from django.db import models
from ckeditor.fields import RichTextField
import datetime
from django.utils import timezone
from django.contrib.auth.models import User

class Professional(models.Model):
    user = models.ForeignKey(User, null=True)
    brand_name = models.CharField(max_length=200,unique=True)
    fullname = models.CharField(max_length=100, blank=True, default="")
    city = models.CharField(max_length=20)
    telephone = models.CharField(max_length=40, blank=True, default="")
    address = models.CharField(max_length=50, blank=True, default="")
    post_code = models.CharField(max_length=10, blank=True, default="")
    facebook = models.CharField(max_length=100, blank=True, default="")
    email = models.CharField(max_length=100, blank=True, default="")
    category = models.CharField(max_length=100)
    latitude = models.CharField(max_length=20, blank=True, default="")
    longitude = models.CharField(max_length=20, blank=True, default="")
    update_news = RichTextField(max_length=1000, blank=True, default="")
    informations = RichTextField(max_length=1000, blank=True, default="")
    url_name = models.CharField(max_length=200,unique=True)
    expire_date = models.DateField('Λήξη εγγραφής')
    update_date = models.DateTimeField('Ενημέρωση νέων')
    subscription = models.CharField(max_length=40, default="free")
    def __str__(self):
        return self.brand_name
    def updated_recently(self):
        return  self.subscription != "free" and self.update_news and (timezone.now() - self.update_date).days < 10