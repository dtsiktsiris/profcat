from django.shortcuts import get_object_or_404, render, redirect
import datetime
from .models import Professional
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from random import shuffle
from django.contrib.auth import authenticate, login, logout

def index(request):
    ret_list = None
    category = ""
    city = ""
    user_urls = None
    if request.user.is_authenticated:
        user_urls = Professional.objects.filter(user = request.user.id)
    if request.method == 'POST':
        if request.POST.get('category') != '' and request.POST.get('city') != '':
            ret_list = Professional.objects.filter(category = request.POST.get('category'),city = request.POST.get('city'))
            category = request.POST.get('category')
            city = request.POST.get('city')
        elif request.POST.get('category') != '':
            ret_list = Professional.objects.filter(category = request.POST.get('category'))
            category = request.POST.get('category')
        elif request.POST.get('city') != '':
            ret_list = Professional.objects.filter(city = request.POST.get('city'))
            city = request.POST.get('city')
    else:
        ret_list = list(Professional.objects.exclude(subscription = "free"))
        shuffle(ret_list)
    city_list = Professional.objects.values_list('city', flat=True).distinct()
    cat_list = Professional.objects.values_list('category', flat=True).distinct()
    return render(request, 'profs/index.html', {'cat_list' : cat_list, 'city_list' : city_list,'ret_list':ret_list, 'category': category,'city':city,'user_urls':user_urls})

def show(request,url_name_para):
    if request.method == 'POST':
        fromaddr = "eviacatalog@gmail.com"
        toaddr = request.POST.get('InputEmail')
        msg = MIMEMultipart('alternative')
        msg.set_charset("utf-8")
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = request.POST.get('InputName')
        body = request.POST.get('InputMessage')
        msg.attach(MIMEText(body, 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, "password")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
      
    prof = get_object_or_404(Professional, url_name=url_name_para)
    return render(request, 'profs/show.html', {'prof': prof})

def contact(request):
    return render(request, 'profs/contact.html')

def check_login(request):
    if request.method == 'POST':
        if request.POST.get('username') != '' and request.POST.get('password') != '' and request.POST.get('password') != 'free':
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            if user is not None:
                login(request, user)
                return redirect('profs:index')
            else:
                return render(request, 'profs/test.html', {'val': 1})
        else:
            return render(request, 'profs/test.html', {'val': 2})
    else:
        return render(request, 'profs/test.html', {'val': 3})

def logout_view(request):
    #if request.session['url_name'] == url_name_para:
    logout(request)
    return redirect('profs:index')

def edit_news(request, url_name_para):
    if request.method == 'POST':
        Professional.objects.filter(url_name=request.POST.get('url_name')).update(update_news=request.POST.get('editor1'),update_date=datetime.datetime.now().date())
        return redirect('profs:index')
    prof = get_object_or_404(Professional, url_name=url_name_para)
    if request.user.is_authenticated and prof.user.id==request.user.id:
        prof = get_object_or_404(Professional, url_name=url_name_para)
        return render(request, 'profs/edit_news.html', {'prof': prof})
    else:
        return redirect('profs:index')